create database qlythuesach1
on
(
name = qlthuesachdata,
filename = 'C:\qlythuesach\qlythuesach1.mdf',
size =2mb,
filegrowth = 10mb
)
log on
(
name = qlthuesachlog,
filename = 'C:\qlythuesach\qlythuesach1log.ldf',
size =2mb,
maxsize = unlimated
)
create table linhvuc
(
malinhvuc char(10) primary key,
tenlinhvuc char(20)
)
create table sachtruyen(
masach char(10) primary key,
tensach char(10) not null,
maloaisach char(10),
malinhvuc char(10),
matg char(10) ,
manxb char(10),
mangonngu char(10),
sotrang int,
giasach int,
dongiathue int,
soluong int,
anh char(10),
ghichu char(20)
)
create table tacgia(
matg char(10) primary key,
tentg char(10) not null,
ngaysinh datetime,
gioitinh char (5),
diachi char(20)
)
create table nhaxuatban(
manxb char(10) primary key,
tennxb char(10),
diachi char(20),
sodt int
)
create table ngonngu(
mangonngu char(10) primary key,
tenngonngu char(10)
)
create table loaisach(
maloaisach char(10) primary key,
tenloaisach char(10)
)
create table trasach(
matra char(10) primary key,
mathue char(10),
manv char(10),
ngaytra datetime,
tongtien int
)
create table calam(
maca char(10) primary key,
tenca char(10)
)
create table thuesach(
mathue char(10) primary key,
makhach char(10),
manv char(10),
ngaythue datetime,
tiendatcoc int
)
create table chitietthuesach(
mathue char(10)primary key,
masach char(10),
matinhtrang char(10),
datra char(10)
)
create table chitiettrasach(
matra char(10)primary key ,
masach char(10),
mavipham char(10),
thanhtien int
)
create table tinhtrang(
matinhtrang char(10) primary key,
tentinhtrang char(10)
)
create table nhanvien(
manv char(10) primary key,
tennv char(10),
maca char(10) ,
namsinh int,
gioitinh char (5),
diachi char(20),
dienthoai int,
luongthang int,
)
create table vipham(
mavipham char(10) primary key,
tenvipham char(10),
tienphat int
)
create table khachhang(
makhach char(10) primary key,
tenkhach char(20),
ngaysinh datetime,
gioitinh char(5),
diachi char(20)
)
--khoa ngoai sach truyen
alter table sachtruyen
add constraint fk_matg foreign key (matg) references tacgia
alter table sachtruyen
add constraint fk_maloaisach foreign key(maloaisach) references loaisach
alter table sachtruyen
add constraint fk_manxb foreign key (manxb) references nhaxuatban
alter table sachtruyen
add constraint fk_mangonngu foreign key(mangonngu) references ngonngu
alter table sachtruyen
add constraint fk_malinhvuc foreign key (malinhvuc) references linhvuc
--khoa ngoai thue sach
alter table thuesach
add constraint fk_makhach foreign key(makhach) references khachhang
alter table thuesach
add constraint fk_manv foreign key(manv) references nhanvien
--khoa ngoai nhan vien
alter table nhanvien
add constraint fk_maca foreign key(maca) references calam
--khoa ngoai chi tiet thue sach
alter table chitietthuesach
add constraint fk_masach foreign key(masach) references sachtruyen
alter table chitietthuesach
add constraint fk_matinhtrang foreign key (matinhtrang) references tinhtrang
--khoa ngoai chi tiet tra sach
alter table chitiettrasach
add constraint fk_masach1 foreign key (masach) references sachtruyen
alter table chitiettrasach
add constraint fk_mavipham foreign key (mavipham) references vipham
--khoa ngoai tra sach
alter table trasach
add constraint fk_mathue foreign key (mathue) references thuesach
alter table trasach
add constraint fk_manv1 foreign key (manv) references nhanvien
--rang buoc
alter table nhanvien
add constraint cc_gioitinh check (gioitinh ='nam' or gioitinh='nu')
alter table loaisach
add constraint cc_tenloai check(tenloaisach='moi' or tenloaisach='cu')
